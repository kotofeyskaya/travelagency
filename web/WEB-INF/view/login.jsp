<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Adventure Time</title>
    <link href="/resources/css/app.css" rel="stylesheet">
</head>
<body id="loginForm">
<div class="login-background"><div></div></div>
<div class="container">
    <div class="login-form">
        <div class="login-div">
            <div class="login-panel">
                <p>Adventure Time</p>
            </div>
            <form id="login" action="/login" method="POST" autocomplete="off">
                <div class="form-group">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Логин">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Пароль">
                </div>
                <button type="submit" class="btn btn-success btn-block" name="submit">Войти</button>
            </form>
            <div class="create-account">
                <p>Еще нет аккаунта? <a href="/singup">Зарегистрируйтесь</a></p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
