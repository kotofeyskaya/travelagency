package com.travelagency.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

@Entity
public class Tour {
    private int tourId;
    private Integer tourTypeId;
    private Integer hotelId;
    private String tourName;
    private Integer price;
    private String city;
    private String country;
    private Date startDate;
    private Date endDate;
    private String description;
    private Collection<Booking> bookingsByTourId;
    private TourType tourTypeByTourTypeId;
    private Hotel hotelByHotelId;

    @Id
    @SequenceGenerator(name="tour_id_tour_seq", sequenceName="tour_id_tour_seq")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tour_id_tour_seq")
    @Column(name = "tour_id", nullable = false)
    public int getTourId() {
        return tourId;
    }

    public void setTourId(int tourId) {
        this.tourId = tourId;
    }

    @Basic
    @Column(name = "tour_type_id")
    public Integer getTourTypeId() {
        return tourTypeId;
    }

    public void setTourTypeId(Integer tourTypeId) {
        this.tourTypeId = tourTypeId;
    }

    @Basic
    @Column(name = "hotel_id")
    public Integer getHotelId() {
        return hotelId;
    }

    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    @Basic
    @Column(name = "tour_name", nullable = false, length = 100)
    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    @Basic
    @Column(name = "price")
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Basic
    @Column(name = "city", length = 100)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "country", length = 100)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "start_date", nullable = false)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "end_date")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "description", length = -1)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tour tour = (Tour) o;

        if (tourId != tour.tourId) return false;
        if (tourTypeId != null ? !tourTypeId.equals(tour.tourTypeId) : tour.tourTypeId != null) return false;
        if (hotelId != null ? !hotelId.equals(tour.hotelId) : tour.hotelId != null) return false;
        if (tourName != null ? !tourName.equals(tour.tourName) : tour.tourName != null) return false;
        if (price != null ? !price.equals(tour.price) : tour.price != null) return false;
        if (city != null ? !city.equals(tour.city) : tour.city != null) return false;
        if (country != null ? !country.equals(tour.country) : tour.country != null) return false;
        if (startDate != null ? !startDate.equals(tour.startDate) : tour.startDate != null) return false;
        if (endDate != null ? !endDate.equals(tour.endDate) : tour.endDate != null) return false;
        if (description != null ? !description.equals(tour.description) : tour.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tourId;
        result = 31 * result + (tourTypeId != null ? tourTypeId.hashCode() : 0);
        result = 31 * result + (hotelId != null ? hotelId.hashCode() : 0);
        result = 31 * result + (tourName != null ? tourName.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "tourByTourId")
    public Collection<Booking> getBookingsByTourId() {
        return bookingsByTourId;
    }

    public void setBookingsByTourId(Collection<Booking> bookingsByTourId) {
        this.bookingsByTourId = bookingsByTourId;
    }

    @ManyToOne
    @JoinColumn(name = "tour_type_id", referencedColumnName = "tour_type_id")
    public TourType getTourTypeByTourTypeId() {
        return tourTypeByTourTypeId;
    }

    public void setTourTypeByTourTypeId(TourType tourTypeByTourTypeId) {
        this.tourTypeByTourTypeId = tourTypeByTourTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "hotel_id", referencedColumnName = "hotel_id")
    public Hotel getHotelByHotelId() {
        return hotelByHotelId;
    }

    public void setHotelByHotelId(Hotel hotelByHotelId) {
        this.hotelByHotelId = hotelByHotelId;
    }
}
