package com.travelagency.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

@Entity
@Table(name = "app_user", schema = "public", catalog = "travelagency")
public class AppUser {
    private int userId;
    private String username;
    private String password;
    private boolean enabled;
    private String personalName;
    private String phone;
    private Date birthDate;
    private String email;
    private Integer roleId;
    private UserRole userRoleByRoleId;
    private Collection<Booking> bookingsByUserId;

    @Id
    @SequenceGenerator(name="user_id_user_seq", sequenceName="user_id_user_seq")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_id_user_seq")
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 20)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = true, length = 100)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "enabled", nullable = false)
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "personal_name", nullable = false, length = 100)
    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    @Basic
    @Column(name = "phone", nullable = true, length = 20)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "birth_date", nullable = true)
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 30)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "role_id", nullable = true)
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppUser appUser = (AppUser) o;

        if (userId != appUser.userId) return false;
        if (enabled != appUser.enabled) return false;
        if (username != null ? !username.equals(appUser.username) : appUser.username != null) return false;
        if (password != null ? !password.equals(appUser.password) : appUser.password != null) return false;
        if (personalName != null ? !personalName.equals(appUser.personalName) : appUser.personalName != null)
            return false;
        if (phone != null ? !phone.equals(appUser.phone) : appUser.phone != null) return false;
        if (birthDate != null ? !birthDate.equals(appUser.birthDate) : appUser.birthDate != null) return false;
        if (email != null ? !email.equals(appUser.email) : appUser.email != null) return false;
        if (roleId != null ? !roleId.equals(appUser.roleId) : appUser.roleId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (personalName != null ? personalName.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (roleId != null ? roleId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
    public UserRole getUserRoleByRoleId() {
        return userRoleByRoleId;
    }

    public void setUserRoleByRoleId(UserRole userRoleByRoleId) {
        this.userRoleByRoleId = userRoleByRoleId;
    }

    @OneToMany(mappedBy = "appUserByUserId")
    public Collection<Booking> getBookingsByUserId() {
        return bookingsByUserId;
    }

    public void setBookingsByUserId(Collection<Booking> bookingsByUserId) {
        this.bookingsByUserId = bookingsByUserId;
    }
}
