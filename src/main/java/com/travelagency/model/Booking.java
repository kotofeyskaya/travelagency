package com.travelagency.model;

import javax.persistence.*;

@Entity
public class Booking {
    private int bookingId;
    private Integer pax;
    private Integer price;
    private Integer userId;
    private Integer tourId;
    private AppUser appUserByUserId;
    private Tour tourByTourId;

    @Id
    @SequenceGenerator(name="booking_id_booking_seq", sequenceName="booking_id_booking_seq")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="booking_id_booking_seq")
    @Column(name = "booking_id", nullable = false)
    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    @Basic
    @Column(name = "pax", nullable = true)
    public Integer getPax() {
        return pax;
    }

    public void setPax(Integer pax) {
        this.pax = pax;
    }

    @Basic
    @Column(name = "price", nullable = true)
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Basic
    @Column(name = "user_id", nullable = true)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "tour_id", nullable = true)
    public Integer getTourId() {
        return tourId;
    }

    public void setTourId(Integer tourId) {
        this.tourId = tourId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Booking booking = (Booking) o;

        if (bookingId != booking.bookingId) return false;
        if (pax != null ? !pax.equals(booking.pax) : booking.pax != null) return false;
        if (price != null ? !price.equals(booking.price) : booking.price != null) return false;
        if (userId != null ? !userId.equals(booking.userId) : booking.userId != null) return false;
        if (tourId != null ? !tourId.equals(booking.tourId) : booking.tourId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = bookingId;
        result = 31 * result + (pax != null ? pax.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (tourId != null ? tourId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public AppUser getAppUserByUserId() {
        return appUserByUserId;
    }

    public void setAppUserByUserId(AppUser appUserByUserId) {
        this.appUserByUserId = appUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "tour_id", referencedColumnName = "tour_id")
    public Tour getTourByTourId() {
        return tourByTourId;
    }

    public void setTourByTourId(Tour tourByTourId) {
        this.tourByTourId = tourByTourId;
    }
}
