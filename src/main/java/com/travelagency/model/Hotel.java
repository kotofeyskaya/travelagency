package com.travelagency.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Hotel {
    private int hotelId;
    private String hotelName;
    private String hotelType;
    private Short starRating;
    private String address;
    private String phone;
    private Collection<Tour> toursByHotelId;

    @Id
    @SequenceGenerator(name="hotel_id_hotel_seq", sequenceName="hotel_id_hotel_seq")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="hotel_id_hotel_seq")
    @Column(name = "hotel_id", nullable = false)
    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    @Basic
    @Column(name = "hotel_name", nullable = true, length = 100)
    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    @Basic
    @Column(name = "hotel_type", nullable = true, length = 30)
    public String getHotelType() {
        return hotelType;
    }

    public void setHotelType(String hotelType) {
        this.hotelType = hotelType;
    }

    @Basic
    @Column(name = "star_rating", nullable = true)
    public Short getStarRating() {
        return starRating;
    }

    public void setStarRating(Short starRating) {
        this.starRating = starRating;
    }

    @Basic
    @Column(name = "address", nullable = true, length = 500)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "phone", nullable = true, length = 11)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hotel hotel = (Hotel) o;

        if (hotelId != hotel.hotelId) return false;
        if (hotelName != null ? !hotelName.equals(hotel.hotelName) : hotel.hotelName != null) return false;
        if (hotelType != null ? !hotelType.equals(hotel.hotelType) : hotel.hotelType != null) return false;
        if (starRating != null ? !starRating.equals(hotel.starRating) : hotel.starRating != null) return false;
        if (address != null ? !address.equals(hotel.address) : hotel.address != null) return false;
        if (phone != null ? !phone.equals(hotel.phone) : hotel.phone != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hotelId;
        result = 31 * result + (hotelName != null ? hotelName.hashCode() : 0);
        result = 31 * result + (hotelType != null ? hotelType.hashCode() : 0);
        result = 31 * result + (starRating != null ? starRating.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "hotelByHotelId")
    public Collection<Tour> getToursByHotelId() {
        return toursByHotelId;
    }

    public void setToursByHotelId(Collection<Tour> toursByHotelId) {
        this.toursByHotelId = toursByHotelId;
    }
}
