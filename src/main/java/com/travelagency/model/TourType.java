package com.travelagency.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "tour_type", schema = "public", catalog = "travelagency")
public class TourType {
    private int tourTypeId;
    private String tourTypeName;
    private Collection<Tour> toursByTourTypeId;

    @Id
    @SequenceGenerator(name="tour_type_id_tour_type_seq", sequenceName="tour_type_id_tour_type_seq")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tour_type_id_tour_type_seq")
    @Column(name = "tour_type_id", nullable = false)
    public int getTourTypeId() {
        return tourTypeId;
    }

    public void setTourTypeId(int tourTypeId) {
        this.tourTypeId = tourTypeId;
    }

    @Basic
    @Column(name = "tour_type_name", nullable = true, length = 100)
    public String getTourTypeName() {
        return tourTypeName;
    }

    public void setTourTypeName(String tourTypeName) {
        this.tourTypeName = tourTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TourType tourType = (TourType) o;

        if (tourTypeId != tourType.tourTypeId) return false;
        if (tourTypeName != null ? !tourTypeName.equals(tourType.tourTypeName) : tourType.tourTypeName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tourTypeId;
        result = 31 * result + (tourTypeName != null ? tourTypeName.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "tourTypeByTourTypeId")
    public Collection<Tour> getToursByTourTypeId() {
        return toursByTourTypeId;
    }

    public void setToursByTourTypeId(Collection<Tour> toursByTourTypeId) {
        this.toursByTourTypeId = toursByTourTypeId;
    }
}
