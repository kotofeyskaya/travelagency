package com.travelagency.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "user_role", schema = "public", catalog = "travelagency")
public class UserRole {
    private int roleId;
    private String role;
    private Collection<AppUser> appUsersByRoleId;

    @Id
    @SequenceGenerator(name="user_role_id_role_seq", sequenceName="user_role_id_role_seq")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_role_id_role_seq")
    @Column(name = "role_id", nullable = false)
    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "role", nullable = false, length = 20)
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRole userRole = (UserRole) o;

        if (roleId != userRole.roleId) return false;
        if (role != null ? !role.equals(userRole.role) : userRole.role != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleId;
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "userRoleByRoleId")
    public Collection<AppUser> getAppUsersByRoleId() {
        return appUsersByRoleId;
    }

    public void setAppUsersByRoleId(Collection<AppUser> appUsersByRoleId) {
        this.appUsersByRoleId = appUsersByRoleId;
    }
}
