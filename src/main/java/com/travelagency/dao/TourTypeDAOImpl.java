package com.travelagency.dao;

import com.travelagency.model.TourType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class TourTypeDAOImpl implements TourTypeDAO {
    @PersistenceContext(unitName = "travelagencyPersistenceUnit")
    private EntityManager entityManager;

    @Override
    public List<TourType> getAll() {
        List<TourType> tourTypes = null;
        try {
            Query query = entityManager.createQuery("SELECT t FROM TourType t");
            tourTypes = query.getResultList();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
        return tourTypes;
    }

    @Override
    public TourType getById(int id) {
        TourType tourType = null;
        try {
            Query query = entityManager.createQuery("SELECT t FROM TourType t WHERE t.tourTypeId =: id").setParameter("id", id);
            tourType = (TourType) query.getSingleResult();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
        return tourType;
    }

    @Override
    public void add(TourType tourType) {
        try {
            entityManager.persist(tourType);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(TourType tourType) {
        try {
            entityManager.merge(tourType);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(TourType tourType) {
        try {
            entityManager.remove(tourType);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        try {
            entityManager.createQuery("DELETE FROM TourType t WHERE t.tourTypeId =: id").setParameter("id", id).executeUpdate();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }
}
