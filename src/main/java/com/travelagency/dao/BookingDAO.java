package com.travelagency.dao;

import com.travelagency.model.Booking;

import java.util.List;

public interface BookingDAO {
    List<Booking> getAll();
    Booking getById(int id);
    void add(Booking booking);
    void update(Booking booking);
    void delete(Booking booking);
    void deleteById(int id);
}
