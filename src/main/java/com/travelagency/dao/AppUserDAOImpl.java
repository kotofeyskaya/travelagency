package com.travelagency.dao;

import com.travelagency.model.AppUser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AppUserDAOImpl implements AppUserDAO {
    @PersistenceContext(unitName = "travelagencyPersistenceUnit")
    private EntityManager entityManager;

    @Override
    public List<AppUser> getAll() {
        List<AppUser> users = null;
        try {
            Query query = entityManager.createQuery("SELECT u FROM AppUser u");
            users = query.getResultList();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public AppUser getById(int id) {
        AppUser appUser = null;
        try {
            Query query = entityManager.createQuery("SELECT u FROM AppUser u WHERE u.userId =: id").setParameter("id", id);
            appUser = (AppUser) query.getSingleResult();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
        return appUser;
    }

    @Override
    public void add(AppUser appUser) {
        try {
            entityManager.persist(appUser);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(AppUser appUser) {
        try {
            entityManager.merge(appUser);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(AppUser appUser) {
        try {
            entityManager.remove(appUser);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        try {
            entityManager.createQuery("DELETE FROM AppUser u WHERE u.userId =: id").setParameter("id", id).executeUpdate();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }
}
