package com.travelagency.dao;

import com.travelagency.model.Tour;

import java.util.List;

public interface TourDAO {
    List<Tour> getAll();
    Tour getById(int id);
    void add(Tour tour);
    void update(Tour tour);
    void delete(Tour tour);
    void deleteById(int id);
}
