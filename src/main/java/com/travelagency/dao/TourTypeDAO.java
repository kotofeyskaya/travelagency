package com.travelagency.dao;

import com.travelagency.model.TourType;

import java.util.List;

public interface TourTypeDAO {
    List<TourType> getAll();
    TourType getById(int id);
    void add(TourType tourType);
    void update(TourType tourType);
    void delete(TourType tourType);
    void deleteById(int id);
}
