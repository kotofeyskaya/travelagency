package com.travelagency.dao;

import com.travelagency.model.AppUser;

import java.util.List;

public interface AppUserDAO {
    List<AppUser> getAll();
    AppUser getById(int id);
    void add(AppUser appUser);
    void update(AppUser appUser);
    void delete(AppUser appUser);
    void deleteById(int id);
}
