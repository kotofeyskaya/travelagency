package com.travelagency.dao;

import com.travelagency.model.Hotel;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class HotelDAOImpl implements HotelDAO {
    @PersistenceContext(unitName = "travelagencyPersistenceUnit")
    private EntityManager entityManager;

    @Override
    public List<Hotel> getAll() {
        List<Hotel> hotels = null;
        try {
            Query query = entityManager.createQuery("SELECT h FROM Hotel h");
            hotels = query.getResultList();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
        return hotels;
    }

    @Override
    public Hotel getById(int id) {
        Hotel hotel = null;
        try {
            Query query = entityManager.createQuery("SELECT h FROM Hotel h WHERE h.hotelId =: id").setParameter("id", id);
            hotel = (Hotel) query.getSingleResult();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
        return hotel;
    }

    @Override
    public void add(Hotel hotel) {
        try {
            entityManager.persist(hotel);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Hotel hotel) {
        try {
            entityManager.merge(hotel);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Hotel hotel) {
        try {
            entityManager.remove(hotel);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        try {
            entityManager.createQuery("DELETE FROM Hotel h WHERE h.hotelId =: id").setParameter("id", id).executeUpdate();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }
}
