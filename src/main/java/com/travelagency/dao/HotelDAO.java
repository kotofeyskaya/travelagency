package com.travelagency.dao;

import com.travelagency.model.Hotel;

import java.util.List;

public interface HotelDAO {
    List<Hotel> getAll();
    Hotel getById(int id);
    void add(Hotel hotel);
    void update(Hotel hotel);
    void delete(Hotel hotel);
    void deleteById(int id);
}
