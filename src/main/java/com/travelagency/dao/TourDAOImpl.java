package com.travelagency.dao;

import com.travelagency.model.Tour;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class TourDAOImpl implements TourDAO {
    @PersistenceContext(unitName = "travelagencyPersistenceUnit")
    private EntityManager entityManager;

    @Transactional
    public List<Tour> getAll() {
        List<Tour> tours = null;
        try {
            Query query = entityManager.createQuery("SELECT t FROM Tour t");
            tours = query.getResultList();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
        return tours;
    }

    @Override
    public Tour getById(int id) {
        Tour tour = null;
        try {
            Query query = entityManager.createQuery("SELECT t FROM Tour t WHERE t.tourId =: id").setParameter("id", id);
            tour = (Tour) query.getSingleResult();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
        return tour;
    }

    @Transactional
    public void add(Tour tour) {
        try {
            entityManager.persist(tour);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void update(Tour tour) {
        try {
            entityManager.merge(tour);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Tour tour) {
        try {
            entityManager.remove(tour);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void deleteById(int id){
        try {
            entityManager.createQuery("DELETE FROM Tour t WHERE t.tourId =: id").setParameter("id", id).executeUpdate();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }
}
