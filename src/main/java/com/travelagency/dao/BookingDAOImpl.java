package com.travelagency.dao;

import com.travelagency.model.Booking;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class BookingDAOImpl implements BookingDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Booking> getAll() {
        List<Booking> bookings = null;
        try {
            Query query = entityManager.createQuery("SELECT b FROM Booking b");
            bookings = query.getResultList();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
        return bookings;
    }

    @Override
    public Booking getById(int id) {
        Booking booking = null;
        try {
            Query query = entityManager.createQuery("SELECT b FROM Booking b WHERE b.bookingId =: id").setParameter("id", id);
            booking = (Booking) query.getSingleResult();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
        return booking;
    }

    @Override
    public void add(Booking booking) {
        try {
            entityManager.persist(booking);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Booking booking) {
        try {
            entityManager.merge(booking);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Booking booking) {
        try {
            entityManager.remove(booking);
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        try {
            entityManager.createQuery("DELETE FROM Booking b WHERE b.bookingId =: id").setParameter("id", id).executeUpdate();
        } catch (TransactionException e) {
            e.printStackTrace();
        }
    }
}
