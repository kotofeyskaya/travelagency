package com.travelagency.service;

import com.travelagency.model.AppUser;

import java.util.List;

public interface UserService {
    List<AppUser> getAll();
    AppUser getById(int id);
    void save(AppUser appUser);
    void delete(AppUser appUser);
    void deleteById(int id);
}
