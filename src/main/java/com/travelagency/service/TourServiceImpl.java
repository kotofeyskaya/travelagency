package com.travelagency.service;

import com.google.common.collect.Lists;
import com.travelagency.dao.TourDAO;
import com.travelagency.model.Tour;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TourServiceImpl implements TourService {
    private TourDAO tourDAO;

    @Autowired
    public TourServiceImpl(TourDAO tourDAO) {
        this.tourDAO = tourDAO;
    }

    @Override
    public List<Tour> getAll() {
        return Lists.newArrayList(tourDAO.getAll());
    }

    @Override
    public Tour getById(int id) {
        return tourDAO.getById(id);
    }

    @Override
    public void save(Tour tour) {
        if (tour.getTourId() > 0) {
            tourDAO.update(tour);
        } else {
            tourDAO.add(tour);
        }
    }

    @Override
    public void delete(Tour tour) {
        tourDAO.delete(tour);
    }

    @Override
    public void deleteById(int id) {
        tourDAO.deleteById(id);
    }
}
