package com.travelagency.service;

import com.travelagency.model.Hotel;

import java.util.List;

public interface HotelService {
    List<Hotel> getAll();
    Hotel getById(int id);
    void save(Hotel hotel);
    void delete(Hotel hotel);
    void deleteById(int id);
}
