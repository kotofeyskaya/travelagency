package com.travelagency.service;

import com.travelagency.model.Booking;

import java.util.List;

public interface BookingService {
    List<Booking> getAll();
    Booking getById(int id);
    void save(Booking booking);
    void delete(Booking booking);
    void deleteById(int id);
}
