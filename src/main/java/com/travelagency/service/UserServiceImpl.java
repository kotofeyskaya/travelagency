package com.travelagency.service;

import com.travelagency.dao.AppUserDAO;
import com.travelagency.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private AppUserDAO appUserDAO;

    @Autowired
    public UserServiceImpl(AppUserDAO appUserDAO) {
        this.appUserDAO = appUserDAO;
    }

    @Override
    public List<AppUser> getAll() {
        return appUserDAO.getAll();
    }

    @Override
    public AppUser getById(int id) {
        return appUserDAO.getById(id);
    }

    @Override
    public void save(AppUser appUser) {
        if (appUser.getUserId() > 0) {
            appUserDAO.update(appUser);
        } else {
            appUserDAO.add(appUser);
        }
    }

    @Override
    public void delete(AppUser appUser) {
        appUserDAO.delete(appUser);
    }

    @Override
    public void deleteById(int id) {
        appUserDAO.deleteById(id);
    }
}
