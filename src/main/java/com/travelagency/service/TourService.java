package com.travelagency.service;

import com.travelagency.model.Tour;

import java.util.List;

public interface TourService {
    List<Tour> getAll();
    Tour getById(int id);
    void save(Tour tour);
    void delete(Tour tour);
    void deleteById(int id);
}
