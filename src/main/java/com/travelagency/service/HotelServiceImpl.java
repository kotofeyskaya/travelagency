package com.travelagency.service;

import com.travelagency.dao.HotelDAO;
import com.travelagency.model.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelServiceImpl implements HotelService {
    private HotelDAO hotelDAO;

    @Autowired
    public HotelServiceImpl(HotelDAO hotelDAO) {
        this.hotelDAO = hotelDAO;
    }

    @Override
    public List<Hotel> getAll() {
        return hotelDAO.getAll();
    }

    @Override
    public Hotel getById(int id) {
        return hotelDAO.getById(id);
    }

    @Override
    public void save(Hotel hotel) {
        if (hotel.getHotelId() > 0) {
            hotelDAO.update(hotel);
        } else {
            hotelDAO.add(hotel);
        }
    }

    @Override
    public void delete(Hotel hotel) {
        hotelDAO.delete(hotel);
    }

    @Override
    public void deleteById(int id) {
        hotelDAO.deleteById(id);
    }
}
