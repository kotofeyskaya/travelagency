package com.travelagency.service;

import com.travelagency.model.TourType;

import java.util.List;

public interface TourTypeService {
    List<TourType> getAll();
    TourType getById(int id);
    void save(TourType tourType);
    void delete(TourType tourType);
    void deleteById(int id);
}
