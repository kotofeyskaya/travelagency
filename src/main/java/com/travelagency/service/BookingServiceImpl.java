package com.travelagency.service;

import com.travelagency.dao.BookingDAO;
import com.travelagency.model.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingServiceImpl implements BookingService {
    private BookingDAO bookingDAO;

    @Autowired
    public BookingServiceImpl(BookingDAO bookingDAO) {
        this.bookingDAO = bookingDAO;
    }

    @Override
    public List<Booking> getAll() {
        return bookingDAO.getAll();
    }

    @Override
    public Booking getById(int id) {
        return bookingDAO.getById(id);
    }

    @Override
    public void save(Booking booking) {
        if (booking.getBookingId() > 0) {
            bookingDAO.update(booking);
        } else {
            bookingDAO.add(booking);
        }
    }

    @Override
    public void delete(Booking booking) {
        bookingDAO.delete(booking);
    }

    @Override
    public void deleteById(int id) {
        bookingDAO.deleteById(id);
    }
}
