package com.travelagency.service;

import com.travelagency.dao.TourTypeDAO;
import com.travelagency.model.TourType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TourTypeServiceImpl implements TourTypeService {
    private TourTypeDAO tourTypeDAO;

    @Autowired
    public TourTypeServiceImpl(TourTypeDAO tourTypeDAO) {
        this.tourTypeDAO = tourTypeDAO;
    }

    @Override
    public List<TourType> getAll() {
        return tourTypeDAO.getAll();
    }

    @Override
    public TourType getById(int id) {
        return tourTypeDAO.getById(id);
    }

    @Override
    public void save(TourType tourType) {
        if (tourType.getTourTypeId() > 0) {
            tourTypeDAO.update(tourType);
        } else {
            tourTypeDAO.add(tourType);
        }
    }

    @Override
    public void delete(TourType tourType) {
        tourTypeDAO.delete(tourType);
    }

    @Override
    public void deleteById(int id) {
        tourTypeDAO.deleteById(id);
    }
}
