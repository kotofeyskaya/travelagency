package com.travelagency.controller;

import com.travelagency.model.Tour;
import com.travelagency.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/api/tours")
public class TourController {
    private TourService tourService;

    @Autowired
    public TourController(TourService tourService) {
        this.tourService = tourService;
    }

    @GetMapping
    public ResponseEntity<List<Tour>> getTours() {
        try {
            return ResponseEntity.ok().body(tourService.getAll());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PostMapping(value = "/save")
    public ResponseEntity saveTour(@RequestBody Tour tour) {
        try {
            tourService.save(tour);
            return ResponseEntity.ok().body("OK");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Oops! Something went wrong.");
        }
    }

//    @PostMapping(value = "/delete")
//    public ResponseEntity deleteTour(@RequestBody Tour tour) {
//        try {
//            tourService.delete(tour);
//            return ResponseEntity.ok().body("OK");
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Oops! Something went wrong.");
//        }
//    }

    @PostMapping(value = "/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int id) {
        try {
            tourService.deleteById(id);
            return ResponseEntity.ok("OK");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Oops! Something went wrong.");
        }
    }
}
