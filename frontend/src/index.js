import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './js/components/app';
import store from './js/store';
import './scss/app.scss';
// import '!style-loader!css-loader!bootstrap/dist/css/bootstrap.css';

ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById('root'));
