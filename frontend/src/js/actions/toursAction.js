import axios from 'axios';

export const GET_TOURS = 'GET_TOURS';
export const CREATE_TOUR = 'CREATE_TOUR';
export const DELETE_TOUR = 'DELETE_TOUR';
export const GET_TOUR = 'GET_TOUR';

const API_URL = 'http://localhost:8080/api/tours';

export function getTours() {
    return (dispatch) => {
        return axios.get(`${API_URL}`)
            .then(response => {
                dispatch(getToursSuccess(response.data))
            })
            .catch(error => {
                throw (error);
            });
    };
}

export function getToursSuccess(tours) {
    return {
        type: GET_TOURS,
        payload: tours
    };
}

// export function createTour(props) {
//     const request = axios.post( `${API_URL}/posts${API_KEY}`, props );
//     const responseText = tourService.addTour(values);
//     return {
//         type: CREATE_TOUR,
//         payload: responseText,
//         props
//     };
// }

// export const createTour = ({ name, price }) => {
//     return async (dispatch) => {
//         try {
//             const response = await axios.post(`${API_URL}/save`, { name, price });
//             dispatch(createTourSuccess(response.data));
//         }
//         catch (error) {
//             throw (error);
//         }
//     };
// };

// export const createTourSuccess = (tour) => {
//     return {
//         type: CREATE_TOUR,
//         payload: {
//             _id: tour._id,
//             name: tour.name,
//             price: tour.price
//         }
//     }
// };


// export function deleteTour(tourId) {
//     return {
//         type: DELETE_TOUR,
//         payload: tourId
//     };
// }

export function deleteTour(id) {
    return (dispatch) => {
        return axios.post(`${API_URL}/delete/${id}`)
            .then(response => {
                dispatch(deleteTourSuccess(response.data))
            })
            .catch(error => {
                throw (error);
            });
    };
};

export function deleteTourSuccess(id) {
    return {
        type: DELETE_TOUR,
        payload: id
    };
}

// export const deleteTourSuccess = id => {
//     return {
//         type: DELETE_TOUR,
//         payload: {
//             id
//         }
//     }
// }

// export function getTour(tourId) {
//     const tour = tourService.getTourById(tourId);
//     return {
//         type: GET_TOUR,
//         payload: tour,
//         tourId
//     };
// }
