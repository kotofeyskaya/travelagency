import React, { Component } from 'react';
import Routes from './routes';
import Header from './header';
import '../../scss/_main.scss';
import '../../scss/_table.scss';

class App extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Routes/>
            </div>
        )
    }
}

export default App;