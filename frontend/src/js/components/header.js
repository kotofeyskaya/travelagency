import React, { Component } from "react";
import '../../scss/_header.scss';

class Header extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg">
                <div className="collapse navbar-collapse justify-content-md-center" id="navbars">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="navbar-brand" href="/">Adventure Time</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/tours">Туры</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/oders">Типы отдыха</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/hotels">Отели</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/clients">Клиенты</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/oders">Заказы</a>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Header;