import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getTours, deleteTour } from '../actions/toursAction';
import moment from 'moment';

class Table extends Component {
    componentWillMount() {
        this.props.getTours();
    }

    onAddClick = () => {
        this.props.getTours();
    }

    onDeleteClick = e => {
        this.props.deleteTour(e);
    }

    onViewClick = e => {

    }

    render() {
        return (
            <div className="pb-4">
                <div className="container">
                    <div className="row">
                        <h2 className="my-2"><i className="fas fa-globe-americas"></i> Туры</h2>
                        <div className="my-2 ml-auto">
                            <button className="btn btn-primary btn-sm btn-add" onClick={() => { this.onAddClick }}><i class="fas fa-plus"></i> Добавить</button>
                        </div>
                    </div>
                    <div className="row">
                        <table className="table table-fixed table-hover">
                            <thead>
                                <tr>
                                    <th className="col-2">Название</th>
                                    <th className="col-1">Цена</th>
                                    <th className="col-2">Город</th>
                                    <th className="col-2">Страна</th>
                                    <th className="col-1">С</th>
                                    <th className="col-1">По</th>
                                    <th className="col-2">Описание</th>
                                    <th className="col-1">Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.tours.map((tour) => {
                                    return (
                                        <tr key={tour.tourId}>
                                            <td className="col-2">{tour.tourName}</td>
                                            <td className="col-1">{tour.price}</td>
                                            <td className="col-2">{tour.city}</td>
                                            <td className="col-2">{tour.country}</td>
                                            <td className="col-1">{moment(tour.startDate).format("YYYY/MM/DD")}</td>
                                            <td className="col-1">{moment(tour.endDate).format("YYYY/MM/DD")}</td>
                                            <td className="col-2">{tour.description}</td>
                                            <td className="col-1">
                                                <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                                    <button className="btn btn-info" onClick={() => { this.onViewClick(tour.tourId) }}><i class="fas fa-pencil-alt"></i></button>
                                                    <button className="btn btn-danger" onClick={() => { this.onDeleteClick(tour.tourId) }}><i class="fas fa-trash"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        tours: state.tours.tours
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getTours: getTours,
        deleteTour: deleteTour
    },
        dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);