import {
    GET_TOURS,
    CREATE_TOUR,
    DELETE_TOUR,
    GET_TOUR
} from '../actions/toursAction';

const INITIAL_STATE = {
    tours: []
};

export default function toursReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        // case CREATE_TOUR:
        //     return [...state, action.payload];
        case DELETE_TOUR:
            // {
            //     const newState = Object.assign([], state);
            //     const indexOfTourToDelete = state.tours.findIndex(tour => {
            //         return tour.tourId == action.payload.tourId
            //     })
            //     newState.splice(indexOfTourToDelete, 1);
            //     return newState;
            // }
        return state.tours.filter(tour => tour.tourId !== action.payload.tourId);
        
        // return tours.filter(tour => tour.tourId !== action.payload.tourId);
        // case GET_TOUR:
        //     return { ...state, tour: action.payload.tour };
        case GET_TOURS:
            return { ...state, tours: action.payload };
        default:
            return state;
    }
}