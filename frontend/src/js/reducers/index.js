import { combineReducers } from 'redux';
import toursReducer from '../reducers/toursReducer';

const rootReducer = combineReducers({
    tours: toursReducer
});

export default rootReducer;