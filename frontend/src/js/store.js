import { createStore, applyMiddleware  } from 'redux';
import rootReducer from './reducers/index';
import thunk from "redux-thunk";
// import { syncHistoryWithStore } from 'react-router-redux';
// import { browserHistory } from 'react-router';

// const defaultState = {
//     tours: [],
//     tourTypes: [],
//     hotels: [],
//     bookings: [],
//     users: [],
//     user: {},
//     isLoggedIn: false
// };

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer,  composeEnhancer(applyMiddleware(thunk)));

// export const history = syncHistoryWithStore(browserHistory, store);

export default store;